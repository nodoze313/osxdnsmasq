
# OSX dnsmasq
function dnsmasq() {
	case "$1" in
		restart)
			dnsmasq stop
			dnsmasq start
			;;
		stop)
			sudo launchctl unload /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
			;;
		start)
			sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
			;;
		conf)
			vim /usr/local/etc/dnsmasq.conf
			;;
		*)
			echo $0." (start|stop|restart|conf)"
			;;
	esac
}

